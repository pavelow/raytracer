/*
CPU Raytracer Interface
Daniel McCarthy 10/18
*/

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Image.hpp>
#include <ctime>
#include <iostream>
#include <glm/glm.hpp>
#include "raymarcher.hpp"

#define Width 480
#define Height 320

int main(int argc, char ** argcv) {
  // create the window
  sf::RenderWindow window(sf::VideoMode(Width, Height), "Ray Tracer",  sf::Style::Titlebar);
  window.setVerticalSyncEnabled(true);

  sf::Sprite sprite;
  sf::Texture texture;
  window.setActive(true);
  bool running = true;



  //Setup RayTracer

  Camera C;
  C.Position = glm::vec3(0,0,0);  // y=1
  C.Direction = glm::vec3(0,0,1); // pointing at +z
  C.FocalDist = 1; //Focal distance

  Tracer T(16, Width, Height, C); //16 Threads
  T.image.create(Width, Height, sf::Color::White); //Image Buffer
  T.StartThreads(); //Start Tracing!

  //End Setup RayTracer

  while(window.isOpen() && running)  {
    sf::Event event;
    while (window.pollEvent(event)) {
      switch(event.type) {
        case sf::Event::Closed:
          running = false;
          break;
        case sf::Event::KeyPressed:
          if(event.key.code == sf::Keyboard::Escape)
            running = false;
            break;
        default:
            break;
          }
    }


    window.clear(sf::Color::Black);
    texture.loadFromImage(T.image); // ¯\_(ツ)_/¯
    sprite.setTexture(texture);
    window.draw(sprite);
    window.display();

    std::this_thread::sleep_for (std::chrono::milliseconds(100)); //Limit loop to 10Hz
  }

  return 0;
}
