#include <glm/glm.hpp>
#include <SFML/Graphics/Image.hpp>
#include <thread>
#include <vector>
#include <stdio.h>
#include <algorithm>
//Number of RayMarch Iterations
#define RayMarches 256

typedef struct Camera {
  glm::vec3 Position;
  glm::vec3 Direction; //Assume up is y+
  float FocalDist;
}Camera;

class Tracer {
public:
  Tracer(uint Nthreads, uint Width, uint Height, Camera Cam);
  void StartThreads();
  void ProcessPixelRange(uint threadIdx);
  sf::Image image;
private:
  std::vector<std::thread> ThreadPool;
  uint Width;
  uint Height;
  uint NThreads;
  Camera Cam;
  float AspectRatio;
  inline float DistanceFunction(glm::vec3 Coord);
  inline sf::Color ComputePixel(uint x, uint y);
};
