#include "raymarcher.hpp"

Tracer::Tracer(uint _NThreads, uint _Width, uint _Height, Camera _Cam) {
  AspectRatio = _Width/_Height;
  Height = _Height;
  Width = _Width;
  NThreads = _NThreads;
  Cam = _Cam;
}

//This is where the magic happens
inline sf::Color Tracer::ComputePixel(uint x, uint y) {
  //Figure out Ray Direction (Vector from camera pos to point on pixel grid `FocalDist` in front of camera pos)
  glm::vec3 PixelPoint = glm::vec3(((float)x-(Width/2.0f))/(Width/2.0f), ((float)y-(Height/2.0f))/(Width/2.0f),0.0f);
  glm::vec3 RayUnit = glm::normalize(PixelPoint + (Cam.Position + glm::vec3(0,0,Cam.FocalDist))); //Calculate Ray Unit Vector
  glm::vec3 Ray = RayUnit;

  //Ray March
  float Distance = 0;
  float IterDist = 0;
  uint iterations = 0;
  do {
    IterDist = DistanceFunction(Ray); //Find Distance from ray end point to /things/
    Distance += IterDist;
    Ray = Cam.Position + RayUnit*Distance; //Find out new ray end point
    iterations++;
  } while(IterDist > 0.005f && iterations < RayMarches); //Stop when we get close enough to something, or take too long (RayMarch Iteration limit)

  if(iterations == RayMarches || Distance != Distance) {
    return sf::Color(0,0,0);
  } else {

    //Calculate Normal - See https://www.shadertoy.com/view/4dSBz3
    glm::vec3 Light = glm::vec3(0,0,-1);

    glm::vec2 e = glm::vec2(0.0005f,-0.0005f); //Arbitrarily small non zero vector

    glm::vec3 exyy = glm::vec3(e.x, e.y, e.y);
    glm::vec3 eyyx = glm::vec3(e.y, e.y, e.x);
    glm::vec3 eyxy = glm::vec3(e.y, e.x, e.y);
    glm::vec3 exxx = glm::vec3(e.x, e.x, e.x);

    glm::vec3 Normal = glm::normalize(
      exyy * DistanceFunction(Ray + exyy) +
      eyyx * DistanceFunction(Ray + eyyx) +
      eyxy * DistanceFunction(Ray + eyxy) +
      exxx * DistanceFunction(Ray + exxx)
    );

    //Diffuse Lighting
    float a = std::clamp(glm::dot(Normal, glm::normalize(-Light-Ray)),0.1f,1.0f);
    a *= 5/glm::dot(Light-Ray, Light-Ray);

    return sf::Color(a*255,a*255,a*255);
  }
}

//Distance Function: See http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
inline float Tracer::DistanceFunction(glm::vec3 Coord) {
  float dist = glm::distance(Coord, glm::vec3(0,0,5))-1.0f; //Sphere of radius 1
  dist = std::min(glm::distance(Coord, glm::vec3(1,0,1.5))-0.5f, dist); //Sphere of radius 0.5
  dist = std::min(glm::distance(Coord, glm::vec3(-1,0,1.5))-0.1f, dist); //Sphere of radius 0.1
  dist = std::min(dist, -Coord.y + 1); //Plane
  return dist;
}

void Tracer::StartThreads() {
  for(uint i=0; i<NThreads; i++) {
    ThreadPool.push_back(std::thread(&Tracer::ProcessPixelRange, this, i)); //Start worker thread
  }
}

void Tracer::ProcessPixelRange(uint threadIdx) {
  uint yFrac = Height/NThreads;
  uint yStart = yFrac*threadIdx;
  uint yStop = yStart+yFrac;

  for (uint y = yStart; y < yStop; y++) {
    for (uint x = 0; x < Width; x++) {
      image.setPixel(x,y, ComputePixel(x,y));
    }
  }
}
